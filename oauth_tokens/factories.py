
from datetime import timedelta

from django.utils import timezone
import factory

from .models import AccessToken, UserCredentials


class UserCredentialsFactory(factory.DjangoModelFactory):
    class Meta:
        model = UserCredentials

    active = True


class AccessTokenFactory(factory.DjangoModelFactory):
    class Meta:
        model = AccessToken

    user_credentials = factory.SubFactory(UserCredentialsFactory)
    expires_at = timezone.now() + timedelta(1)
